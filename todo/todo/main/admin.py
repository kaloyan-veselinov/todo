# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import Category, Task, TaskFile


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ('name',)
    search_fields = ('name',)


@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'parent',
        'category',
        'due_date',
        'priority',
        'done'
    )
    list_filter = ('parent', 'category', 'created', 'modified', 'due_date', 'done')
    search_fields = ('name',)


@admin.register(TaskFile)
class TaskFileAdmin(admin.ModelAdmin):
    list_display = ('id', 'created', 'modified', 'task', 'file')
    list_filter = ('created', 'modified', 'task')
