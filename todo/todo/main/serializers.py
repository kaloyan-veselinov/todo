from rest_framework import serializers

from .models import Task, Category, TaskFile


class NestedTaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = Task
        exclude = ('details', 'parent')


class NestedTaskFileSerializer(serializers.ModelSerializer):
    class Meta:
        model = TaskFile
        exclude = ('task', 'id', 'created', 'modified')


class TaskSerializer(serializers.ModelSerializer):
    subtasks = NestedTaskSerializer(many=True, read_only=True)
    files = NestedTaskFileSerializer(many=True, read_only=True)

    class Meta:
        model = Task
        fields = '__all__'


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'


class TaskFileSerializer(serializers.ModelSerializer):
    class Meta:
        model = TaskFile
        fields = '__all__'
