from django.test import TestCase

from .models import Task, Category


class TasksTestCase(TestCase):
    def setUp(self):
        self.parent_category = Category.objects.create(name="Category 1")
        self.child_category = Category.objects.create(name="Category 2")
        self.parent_task = Task.objects.create(
            name="Parent task",
            category=self.parent_category
        )

    def test_parent_category_autoset(self):
        child_task = Task.objects.create(
            name="Child taks",
            category=self.child_category,
            parent=self.parent_task
        )
        self.assertEquals(child_task.category, self.parent_task.category)
