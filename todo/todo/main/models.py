from datetime import date
from os.path import join

from django.db import models
from django.db.models.signals import pre_save, pre_delete
from django.dispatch import receiver
from django_extensions.db.models import TimeStampedModel
from django.utils.translation import gettext_lazy as _


class Category(models.Model):
    name = models.CharField("Name", max_length=50, unique=True)

    class Meta:
        verbose_name_plural = "Categories"

    def __str__(self) -> str:
        return f"Category <{self.name}>"


class Task(TimeStampedModel):
    class Priorities(models.IntegerChoices):
        HIGH = 1, _("High")
        MID = 2, _("Medium")
        LOW = 3, _("Low")

    name = models.CharField("Name", max_length=50)
    category = models.ForeignKey(Category, on_delete=models.CASCADE, related_name="tasks", null=True, blank=True)
    parent = models.ForeignKey("self", on_delete=models.CASCADE, related_name="subtasks", null=True, blank=True)
    details = models.TextField("Description", null=True, blank=True)
    due_date = models.DateTimeField("Due date", null=True, blank=True)
    priority = models.IntegerField("Priority", choices=Priorities.choices, default=Priorities.MID)
    done = models.BooleanField("Done", default=False)

    def __str__(self) -> str:
        return f"Task <{self.id} - {self.name}>"


def task_file_path(instance, filename):
    return join('tasks', str(instance.task.id), filename)


class TaskFile(TimeStampedModel):
    task = models.ForeignKey(Task, on_delete=models.CASCADE, related_name="files")
    file = models.FileField("Uploaded file for task", upload_to=task_file_path)


@receiver(pre_delete, sender=TaskFile)
def delete_outline_on_parcel_deleted(sender, instance: TaskFile, **kwargs):
    instance.file.delete()
