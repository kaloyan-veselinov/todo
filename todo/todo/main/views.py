from django.http import HttpRequest, HttpResponse
from django.shortcuts import render, get_object_or_404
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from .models import Task, Category, TaskFile
from .serializers import CategorySerializer, TaskSerializer, TaskFileSerializer


def hello(request: HttpRequest) -> HttpResponse:
    return HttpResponse('Hello, world !')


def all_tasks(request: HttpRequest) -> HttpResponse:
    tasks = Task.objects.all()
    return render(request, 'main/all_tasks.html', locals())


def task_detail(request: HttpRequest, pk: int) -> HttpResponse:
    task = get_object_or_404(Task, id=pk)
    return render(request, 'main/task.html', locals())


class CategoryViewset(viewsets.ModelViewSet):
    serializer_class = CategorySerializer
    queryset = Category.objects.order_by('name')


class TaskViewset(viewsets.ModelViewSet):
    serializer_class = TaskSerializer
    queryset = Task.objects.order_by('priority', 'due_date', 'id')
    filter_backends = (DjangoFilterBackend,)
    filter_fields = ('category', 'parent', 'done')

    @action(detail=False, methods=['get', 'post'])
    def root(self, request):
        root_tasks = self.get_queryset().filter(parent=None)
        return Response(self.get_serializer(root_tasks, many=True).data)


class TaskFileViewset(viewsets.ModelViewSet):
    serializer_class = TaskFileSerializer
    queryset = TaskFile.objects.order_by('task__id')