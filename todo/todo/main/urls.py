"""todo URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path, include
from rest_framework.routers import DefaultRouter

from . import views
from .views import CategoryViewset, TaskViewset, TaskFileViewset

router = DefaultRouter()
router.register(r'categories', CategoryViewset)
router.register(r'tasks', TaskViewset)
router.register(r'files', TaskFileViewset)


urlpatterns = [
    path('hello/', views.hello, name='hello'),
    path('tasks/<int:pk>/', views.task_detail, name='task_details'),
    path('tasks/', views.all_tasks, name='all_tasks'),
    path('api/v1/', include(router.urls)),
]
